from Nirmitee.utils import XMLtoXLSXConverter

xml_converter = XMLtoXLSXConverter('Input.xml','Response.xlsx')

output_xlsx_file = xml_converter.process_xml_to_xlsx()

print(f"***Data successfully written to {output_xlsx_file}***")