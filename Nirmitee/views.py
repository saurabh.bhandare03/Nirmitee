from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import FileResponse
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework import status
from rest_framework.permissions import BasePermission
from rest_framework.parsers import FileUploadParser,JSONParser,MultiPartParser
from .utils import XMLtoXLSXConverter
from .models import *

class BypassAuthentication(BasePermission):
    def has_permission(self, request, view):
        return True

class parse_xml_xls(APIView):

	permission_classes = [BypassAuthentication]
	parser_classes = (JSONParser, MultiPartParser)

	def get(self,request):
		temp = Transaction.objects.none()
		response={'status':'ok'}
		return Response(response)

	def post(self, request):
		if 'xml_file' not in request.FILES:
			return Response({"error": "No XML file provided"}, status=status.HTTP_400_BAD_REQUEST)
		
		xml_file = request.FILES['xml_file']
		temp_xml_file_path = 'temp.xml'

		with open(temp_xml_file_path, 'wb') as temp_xml_file:
			for chunk in xml_file.chunks():
				temp_xml_file.write(chunk)

		xml_converter = XMLtoXLSXConverter(temp_xml_file_path,'Response.xlsx')
		output_xlsx_file = xml_converter.process_xml_to_xlsx()
		
		with open(output_xlsx_file, 'rb') as xlsx_file:
			response = HttpResponse(xlsx_file.read(), content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
			response['Content-Disposition'] = f'attachment; filename=output.xlsx'
		os.remove(temp_xml_file_path)
		os.remove(output_xlsx_file)
		
		return response

