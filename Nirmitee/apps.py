from django.apps import AppConfig


class NirmiteeConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Nirmitee'
