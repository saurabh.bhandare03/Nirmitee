import openpyxl
from openpyxl.styles import Font
import xml.etree.ElementTree as ET

class XMLtoXLSXConverter:
    def __init__(self, xml_file_path,output_file_path):
        self.xml_file_path = xml_file_path
        self.output_file_path = output_file_path

    def process_xml_to_xlsx(self):
        tree = ET.parse(self.xml_file_path)
        root = tree.getroot()
        workbook = openpyxl.Workbook()
        worksheet = workbook.active
        header = ['Date', 'Transaction Type', 'Vch No.', 'Ref No', 'Ref Type', 'Ref Date', 'Debtor', 'Ref Amount', 'Amount', 'Particulars', 'Vch Type', 'Amount Verified']
        bold_font = Font(bold=True)
        for col, title in enumerate(header, start=1):
            cell = worksheet.cell(row=1, column=col, value=title.title())
            cell.font = bold_font
        row_idx = 2
        for voucher in root.findall('.//VOUCHER'):
            voucher_type = voucher.find('VOUCHERTYPENAME').text
            if voucher_type == "Receipt":
                date = voucher.find('DATE').text
                vch_no = voucher.find('VOUCHERNUMBER').text
                ref_elem = voucher.find('REFERENCE')
                ref_no = ref_elem.text if ref_elem is not None else ""
                ref_type = "Receipt"  
                ref_date_elem = voucher.find('REFERENCEDATE')
                ref_date = ref_date_elem.text if ref_date_elem is not None else ""
                debtor = voucher.find('PARTYLEDGERNAME').text
                ref_amount_elem = voucher.find('.//ALLLEDGERENTRIES.LIST/AMOUNT')
                ref_amount = ref_amount_elem.text if ref_amount_elem is not None else ""
                amount = ref_amount 
                narration_elem = voucher.find('NARRATION')
                narration = narration_elem.text if narration_elem is not None else ""
                vch_type = voucher.find('VOUCHERTYPENAME').text
                amount_verified = "Yes"  # Assuming all amounts are verified

                # Write the extracted information to the Excel file
                row_data = [date, "Receipt", vch_no, ref_no, ref_type, ref_date, debtor, ref_amount, amount, narration, vch_type, amount_verified]
                for col_idx, value in enumerate(row_data, start=1):
                    cell = worksheet.cell(row=row_idx, column=col_idx, value=value)
                row_idx += 1

        # Save the workbook
        workbook.save(self.output_file_path)

        return self.output_file_path


