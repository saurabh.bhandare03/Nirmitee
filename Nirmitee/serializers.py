from rest_framework import serializers

class TransactionSerializer(serializers.Serializer):
    date = serializers.DateField()
    transaction_type = serializers.CharField()
    vch_no = serializers.CharField()
    ref_no = serializers.CharField()
    ref_type = serializers.CharField()
    ref_date = serializers.DateField()
    debtor = serializers.CharField()
    ref_amount = serializers.DecimalField(max_digits=10, decimal_places=2)
    amount = serializers.DecimalField(max_digits=10, decimal_places=2)
    particulars = serializers.CharField()
    vch_type = serializers.CharField()
    amount_verified = serializers.BooleanField()