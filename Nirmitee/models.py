from django.db import models

class Transaction(models.Model):
    date = models.DateField()
    transaction_type = models.CharField(max_length=100)
    vch_no = models.CharField(max_length=100)
    ref_no = models.CharField(max_length=100)
    ref_type = models.CharField(max_length=100)
    ref_date = models.DateField()
    debtor = models.CharField(max_length=100)
    ref_amount = models.DecimalField(max_digits=10, decimal_places=2)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    particulars = models.TextField()
    vch_type = models.CharField(max_length=100)
    amount_verified = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.vch_no} - {self.date}"
