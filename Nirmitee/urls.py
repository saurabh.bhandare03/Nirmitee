# log/urls.py
#from django.conf.urls import include, url
from django.urls import include, path
from django.contrib import admin
# Add this import
from django.contrib.auth import views
#from AmigoApp.autocompleteview import TopicsAutocomplete
from .views import *

# We are adding a URL called /home
urlpatterns = [
    #path('parse/', parse_xml, name='parseXML'),
  path('xml/', parse_xml_xls.as_view()),
]
#   